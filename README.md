# openca
An open-source, GLFW/OpenGL Cellular Automaton program, with lua-defined
rulesets, and capability of arbitrary numbers of states.  Currently only
supporting a 2D grid, though arbitrary grids are planned.

# License
This program is licensed under the ISC license (used by the OpenBSD Project)

This readme is licensed under the GNU FDL version 1.3 (as stated in the very
foot of this document).

Copyright (C) 2015 Taylor C. Richberger
Permission is granted to copy, distribute and/or modify this document under the
terms of the GNU Free Documentation License, Version 1.3 or any later version
published by the Free Software Foundation; with no Invariant Sections, no
Front-Cover Texts, and no Back-Cover Texts.  A copy of the license is included
in the section entitled "GNU Free Documentation License".
