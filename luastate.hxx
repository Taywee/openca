/*
 * This file is part of openca.
 *
 * Copyright (c) 2015, Taylor C. Richberger <taywee@gmx.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#pragma once

#include <lua.hpp>

/** Simple Lua state management wrapper.
 * Creates state, converts to the state easily, and deletes the state through RAII.
 */
class LuaState
{
    private:
        lua_State *lua;

    public:
        /// Create the state.
        LuaState();

        /// The state should not be copied for any reason.
        LuaState(const LuaState &that) = delete;

        /// The state can be moved in a trivial way.
        LuaState(LuaState &&that);

        /// Close the Lua state if it exists
        ~LuaState();

        /// Cast easily to the Lua state for Lua calls
        operator lua_State *();

        // Error Callback function
        static int Error(lua_State *l);
};
