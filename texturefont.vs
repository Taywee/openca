#version 330
/*
 * This file is part of openca.
 *
 * Copyright (c) 2015, Taylor C. Richberger <taywee@gmx.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

uniform vec2 screenSize;
uniform float fontSize;

in vec2 vert;

// instance attributes
in int charIndex;

// Dimensions in unit size
in vec2 dimensions;

// Bearing in unit size
in vec2 bearing;

// Position in pixels
in vec2 position;

// Output character index
flat out int charInd;

// Output texture coordinates 
out vec2 texCoords;

void main()
{
    charInd = charIndex;
    // vertex               texcoords
    // [-1,  1] [ 1,  1]    [0, 0] [1, 0]
    // [-1, -1] [ 1, -1]    [0, 1] [1, 1]
    // ((x, -y) + 1.0) / 2.0
    // The (1.0 + 2.0 / fontSize) multiplication is to fix the "flat-edge" look on characters that hit the edge of their bitmaps.  It essentially gives a 1 pixel border on each side.
    texCoords = (((1.0 + 2.0 / fontSize) * vec2(vert.x, -vert.y) + 1.0) / 2.0) * dimensions;
    //texCoords = ((vec2(vert.x, -vert.y) + 1.0) / 2.0) * dimensions;

    // Position of the text in clipping coordinates
    //vec2 origin = ((position / screenSize) - 0.5) * 2.0;
    // The height and width.  Not multiplying by 2 because the vertex coordinates are already 2 width and height.
    //vec2 clipDimensions = dimensions * fontSize / screenSize;
    // Position of vertex as shown as bottom right of origin
    //vec2 originPosition = vec2(origin.x + clipDimensions.x, origin.y - clipDimensions.y);
    //vec2 clipBearing = (bearing * fontSize / screenSize) * 2.0;
    //gl_Position = vec4(vert * clipDimensions + originPosition + clipBearing, 0.25, 1.0);

    // The previous calculations were simplified into this one
    gl_Position = vec4(((((vert * dimensions) + vec2(dimensions.x, -dimensions.y) + (bearing * 2.0)) * fontSize) + (2.0 * position)) / screenSize - 1.0, 0.25, 1.0);
}
