/*
 * This file is part of openca.
 *
 * Copyright (c) 2015, Taylor C. Richberger <taywee@gmx.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "gamecore.hxx"

#include <stdexcept>
#include <iostream>

#include "state.hxx"
#include "menustate.hxx"

// Initialize frameTime to 1/60 of a second so that the first frame doesn't divide by zero
GameCore::GameCore() : window(nullptr), frameTime(1.0 / 60.0), runTime(0.0), windowSize(500, 500)
{
    glfwSetErrorCallback(ErrorCallback);

    if (!glfwInit())
    {
        throw (std::runtime_error("Could not init GLFW"));
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, gl::TRUE_);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


    window = glfwCreateWindow(std::get<0>(windowSize), std::get<1>(windowSize), "openca", nullptr, nullptr);
    if (!window)
    {
        glfwTerminate();
        throw (std::runtime_error("Could not create GLFW window"));
    }
    glfwSetWindowUserPointer(window, this);
    glfwSetWindowSizeCallback(window, Callback<int, int>::Function<&GameCore::WindowSizeCallback>);
    glfwSetKeyCallback(window, Callback<int, int, int, int>::Function<&GameCore::KeyCallback>);
    glfwSetMouseButtonCallback(window, Callback<int, int, int>::Function<&GameCore::MouseButtonCallback>);

    glfwMakeContextCurrent(window);

    glfwSwapInterval(1);

    if (!gl::sys::LoadFunctions())
    {
        glfwDestroyWindow(window);
        glfwTerminate();
        throw (std::runtime_error("Could not init ogl"));
    }

    gl::Enable(gl::DEPTH_TEST);
    gl::Enable(gl::BLEND);
    gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
    gl::ClearColor(0.0, 0.0, 0.0, 1.0);
    texturefontbuffer = std::unique_ptr<TextureFontBuffer>(new TextureFontBuffer("DroidSans.ttf"));
    PushState(new MenuState(this, *texturefontbuffer.get()));
}

GameCore::~GameCore()
{
    glfwDestroyWindow(window);
    glfwTerminate();
}

void GameCore::Run()
{
    while (!(glfwWindowShouldClose(window) || states.empty()))
    {
        Process();

        Render();

        glfwPollEvents();

        GLErrors();

        deadStates.clear();
    }
}

void GameCore::Process()
{
    states.back()->Process(frameTime);
}

void GameCore::Render()
{
    gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
    states.back()->Render();
    glfwSwapBuffers(window);

    // Only call glfwGetTime once
    const double currentTime = glfwGetTime();
    frameTime = currentTime - runTime;
    runTime = currentTime;
}

void GameCore::GLErrors()
{
    GLenum error(gl::GetError());
    while (error != gl::NO_ERROR_)
    {
        switch (error)
        {
            case gl::INVALID_OPERATION:
                {
                    std::cout << "OpenGL Error: INVALID_OPERATION" << std::endl;
                    break;
                }
            case gl::INVALID_ENUM:
                {
                    std::cout << "OpenGL Error: INVALID_ENUM" << std::endl;
                    break;
                }
            case gl::INVALID_VALUE:
                {
                    std::cout << "OpenGL Error: INVALID_VALUE" << std::endl;
                    break;
                }
            case gl::OUT_OF_MEMORY:
                {
                    std::cout << "OpenGL Error: OUT_OF_MEMORY" << std::endl;
                    break;
                }
            case gl::INVALID_FRAMEBUFFER_OPERATION:
                {
                    std::cout << "OpenGL Error: INVALID_FRAMEBUFFER_OPERATION" << std::endl;
                    break;
                }
            default:
                {
                    std::cout << "OpenGL Error: DEFAULT ERROR" << std::endl;
                    break;
                }
        }
        error = gl::GetError();
    }
}

void GameCore::PushState(State *state)
{
    if (!states.empty())
    {
        states.back()->Pause();
    }

    // vector::emplace_back can throw, which would cause a leak
    std::unique_ptr<State> stateptr(state);
    states.push_back(std::move(stateptr));
}

void GameCore::PopState()
{
    // deadStates is used here because PopState is always called by the back
    // state.  Because the object is committing suicide, it can cause a
    // segfault if the object attempts to access its own members (or those of a
    // base class) after popping itself.  Using the deadStates structure and
    // then deleting it in the base loop prevents this.
    deadStates.emplace_back(std::move(states.back()));

    states.pop_back();

    if (!states.empty())
    {
        states.back()->Resume();
    }
}

const std::tuple<int, int> &GameCore::GetWindowSize()
{
    return windowSize;
}

void GameCore::ErrorCallback(int error, const char * description)
{
    std::cerr << "GLFW Error " << error << ": " << description << std::endl;
}

void GameCore::KeyCallback(int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS || action == GLFW_REPEAT)
    {
        if (!states.empty())
        {
            states.back()->KeyPress(key, mods & GLFW_MOD_SHIFT);
        }
    }
}

void GameCore::MouseButtonCallback(int button, int action, int mods)
{
    if (action == GLFW_PRESS)
    {
        double xpos, ypos;
        int xsize, ysize;

        glfwGetCursorPos(window, &xpos, &ypos);
        glfwGetWindowSize(window, &xsize, &ysize);

        const double x = xpos / static_cast<double>(xsize);
        const double y = (static_cast<double>(ysize) - ypos) / static_cast<double>(ysize);

        if (!states.empty())
        {
            states.back()->MousePress(x, y);
        }
    }
}

void GameCore::WindowSizeCallback(int width, int height)
{
    std::get<0>(windowSize) = width;
    std::get<1>(windowSize) = height;
    gl::Viewport(0, 0, width, height);
    if (!states.empty())
    {
        states.back()->WindowSize(width, height);
    }
}
