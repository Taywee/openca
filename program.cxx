/*
 * This file is part of openca.
 *
 * Copyright (c) 2015, Taylor C. Richberger <taywee@gmx.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "program.hxx"

#include <vector>
#include <string>
#include <stdexcept>
#include <iostream>

Program::~Program()
{
    gl::DeleteProgram(program);
}

void Program::AttachShader(Shader &shader)
{
    gl::AttachShader(program, shader.shader);
}

void Program::DetachShader(Shader &shader)
{
    gl::DetachShader(program, shader.shader);
}

void Program::Link()
{
    gl::LinkProgram(program);
    GLint linked = 0;
    gl::GetProgramiv(program, gl::LINK_STATUS, &linked);
    if(!linked)
    {
        GLint logLength = 0;
        gl::GetProgramiv(program, gl::INFO_LOG_LENGTH, &logLength);

        std::vector<GLchar> errorLog(logLength, '\0');
        gl::GetProgramInfoLog(program, logLength, nullptr, errorLog.data());
        while (errorLog.back() == '\0')
        {
            errorLog.pop_back();
        }

        std::string error("Could not link program:\n");
        error.append(errorLog.begin(), errorLog.end());

        gl::DeleteProgram(program);

        throw std::runtime_error(error);
    }

}

void Program::Use()
{
    gl::UseProgram(program);
}

GLuint Program::Get()
{
    return program;
}
