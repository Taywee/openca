/*
 * This file is part of openca.
 *
 * Copyright (c) 2015, Taylor C. Richberger <taywee@gmx.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#pragma once

#include <locale>
#include <vector>
#include <string>

#include "state.hxx"
#include "texturefont.hxx"
#include "texturefontbuffer.hxx"

class MenuState : public State
{
    private:
        TextureFontBuffer &font;
        const std::locale locale;

        // Color differences necessitate separate fonts
        // They all share one buffer
        TextureFont startFont;
        TextureFont automatonFont;
        TextureFont wrapFont;
        TextureFont exitFont;
        TextureFont instructionsFont;

        static constexpr unsigned char menuitems = 4;
        signed char currentItem;
        bool wrap;
        unsigned int width;

        std::vector<std::string> automatons;
        // automatons will not change after setup, so this is fine
        std::vector<std::string>::const_iterator selectedAutomaton;

    public:
        MenuState(GameCore *game, TextureFontBuffer &font);
        virtual ~MenuState();

        virtual void Pause() override;
        virtual void Resume() override;

        virtual void Process(double frameTime) override;
        virtual void Render() override;

        virtual void KeyPress(const int key, const bool shift) override;

        virtual void WindowSize(int width, int height) override;
};
