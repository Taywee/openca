/*
 * This file is part of openca.
 *
 * Copyright (c) 2015, Taylor C. Richberger <taywee@gmx.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "mainstate.hxx"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stdexcept>
#include <random>

#include "shader.hxx"

MainState::MainState(GameCore *game, TextureFontBuffer &texturefontbuffer, const std::string &automaton, const unsigned int width, const bool wrap) : State(game), locale(""), font(texturefontbuffer), iteration(0), width(width), widthsquared(width * width), frequency(10), interval(1.0 / static_cast<double>(frequency)), passedTime(0.0), paused(false), refresh(true)
{
    luaL_openlibs(lua);

    if (luaL_loadfile(lua, automaton.c_str()) || lua_pcall(lua, 0, 0, 0))
    {
        throw std::runtime_error(std::string(lua_tostring(lua, -1)));
    }
    // remove the unnecessary chunk from the stack
    lua_settop(lua, 0);

    // The run function will always be the first
    lua_getglobal(lua, "RunCells");
    if (!lua_isfunction(lua, 1))
    {
        throw std::runtime_error(automaton + " does not have a RunCells function");
    }

    lua_getglobal(lua, "states");
    if (!lua_istable(lua, 2))
    {
        throw std::runtime_error(automaton + " does not have a states table");
    }

    for (GLubyte state = 1;; ++state)
    {
        lua_rawgeti(lua, 2, state);

        if (lua_isnil(lua, 3))
        {
            lua_pop(lua, 1);
            break;
        }

        lua_getfield(lua, 3, "r");
        lua_getfield(lua, 3, "g");
        lua_getfield(lua, 3, "b");

        states.emplace_back(lua_tonumber(lua, 4), lua_tonumber(lua, 5), lua_tonumber(lua, 6));

        lua_pop(lua, 4);
    }
    lua_pop(lua, 1);

    // The cell table will always be the second in the stack
    lua_createtable(lua, widthsquared, 0);

    // First loop creates tables
    for (unsigned int cell = 1; cell <= widthsquared; ++cell)
    {
        lua_createtable(lua, 8, 3);
        lua_pushinteger(lua, 0);
        lua_setfield(lua, 3, "state");
        lua_pushinteger(lua, 0);
        // First iteration will kill state otherwise
        lua_setfield(lua, 3, "nextState");
        lua_pushinteger(lua, cell);
        // For possible tests
        lua_setfield(lua, 3, "cell");
        lua_rawseti(lua, 2, cell);
    }

    // Second loop links neighbors
    for (unsigned int y = 0; y < width; ++y)
    {
        for (unsigned int x = 0; x < width; ++x)
        {
            // 1-indexed cell number in table
            const unsigned int cell = y * width + x + 1;
            lua_rawgeti(lua, 2, cell);

            // Coordinates go counterclockwise, starting at the right
            const static char coords[8][2] = {
                {1, 0}, {1, 1}, {0, 1}, {-1, 1}, {-1, 0}, {-1, -1}, {0, -1}, {1, -1}
            };

            for (unsigned char place = 0; place < 8; ++place)
            {
                if (wrap)
                {
                    const unsigned int nx = (x + width + coords[place][0]) % width;
                    const unsigned int ny = (y + width + coords[place][1]) % width;
                    const unsigned int ncell = ny * width + nx + 1;
                    lua_rawgeti(lua, 2, ncell);
                    lua_rawseti(lua, 3, place + 1);
                } else
                {
                    const unsigned int nx = x + coords[place][0];
                    const unsigned int ny = y + coords[place][1];
                    // Wrapping takes care of negative terms
                    if (nx >= width || ny >= width)
                    {
                        lua_pushboolean(lua, false);
                        lua_rawseti(lua, 3, place + 1);
                    } else
                    {
                        const unsigned int ncell = ny * width + nx + 1;
                        lua_rawgeti(lua, 2, ncell);
                        lua_rawseti(lua, 3, place + 1);
                    }
                }
            }
            lua_pop(lua, 1);
        }
    }

    std::string vertsrc;
    std::string fragsrc;
    {
        std::ifstream shaderFile("square.vs");
        std::ostringstream buffer;
        buffer << shaderFile.rdbuf();
        vertsrc.assign(buffer.str());
    }
    {
        std::ifstream shaderFile("square.fs");
        std::ostringstream buffer;
        buffer << shaderFile.rdbuf();
        fragsrc.assign(buffer.str());
    }

    Shader vert(gl::VERTEX_SHADER, vertsrc);
    Shader frag(gl::FRAGMENT_SHADER, fragsrc);

    program = std::unique_ptr<Program>(new Program(vert, frag));

    GLfloat vertices[12] = {
        1.0, 1.0,
        -1.0, 1.0,
        -1.0, -1.0,
        1.0, 1.0,
        -1.0, -1.0,
        1.0, -1.0
    };

    gl::GenVertexArrays(1, &squareVAO);
    gl::BindVertexArray(squareVAO);

    gl::GenBuffers(1, &squareVBO);

    gl::BindBuffer(gl::ARRAY_BUFFER, squareVBO);
    gl::BufferData(gl::ARRAY_BUFFER, sizeof(vertices), vertices, gl::STATIC_DRAW);
    GLint vertAttrib = gl::GetAttribLocation(program->Get(), "vert");
    gl::VertexAttribPointer(vertAttrib, 2, gl::FLOAT, gl::FALSE_, 0, 0);
    gl::EnableVertexAttribArray(vertAttrib);

    gl::GenBuffers(1, &stateVBO);

    gl::BindBuffer(gl::ARRAY_BUFFER, stateVBO);
    gl::BufferData(gl::ARRAY_BUFFER, sizeof(GLubyte) * widthsquared, nullptr, gl::DYNAMIC_DRAW);
    vertAttrib = gl::GetAttribLocation(program->Get(), "state");
    gl::VertexAttribIPointer(vertAttrib, 1, gl::UNSIGNED_BYTE, 0, 0);
    gl::VertexAttribDivisor(vertAttrib, 1);
    gl::EnableVertexAttribArray(vertAttrib);

    program->Use();
    GLint uniform = gl::GetUniformLocation(program->Get(), "width");
    gl::Uniform1i(uniform, width);

    std::vector<GLfloat> statesArr;
    for (const auto &state: states)
    {
        statesArr.push_back(std::get<0>(state));
        statesArr.push_back(std::get<1>(state));
        statesArr.push_back(std::get<2>(state));
    }
    uniform = gl::GetUniformLocation(program->Get(), "states");
    gl::Uniform3fv(uniform, states.size(), statesArr.data());

    gl::BindBuffer(gl::ARRAY_BUFFER, 0);
    gl::BindVertexArray(0);

    font.SetColor(0.0, 0.0, 1.0);
    const std::tuple<int, int> &windowSize = GetWindowSize();
    font.WindowSize(std::get<0>(windowSize), std::get<1>(windowSize));
}

MainState::~MainState()
{
    gl::DeleteBuffers(1, &squareVBO);
    gl::DeleteBuffers(1, &stateVBO);
    gl::DeleteVertexArrays(1, &squareVAO);
}

void MainState::Pause()
{
}

void MainState::Resume()
{
}

void MainState::Process(double frameTime)
{
    if (!paused)
    {
        passedTime += frameTime;

        while (passedTime >= interval)
        {
            passedTime -= interval;
            lua_pushvalue(lua, 1);
            lua_pushvalue(lua, 2);

            if (lua_pcall(lua, 1, 0, 0))
            {
                throw std::runtime_error(std::string(lua_tostring(lua, -1)));
            }

            ++iteration;
            refresh = true;
        }
    }
}

void MainState::Render()
{
    gl::BindVertexArray(squareVAO);

    if (refresh)
    {
        // Push difference to the GPU
        GLubyte states[widthsquared];

        for (unsigned int cell = 0; cell < widthsquared; ++cell)
        {
            lua_rawgeti(lua, 2, cell + 1);
            lua_getfield(lua, 3, "state");
            const GLubyte state = lua_tointeger(lua, 4);
            lua_pop(lua, 2);

            states[cell] = state;
        }
        gl::BindBuffer(gl::ARRAY_BUFFER, stateVBO);
        gl::BufferData(gl::ARRAY_BUFFER, sizeof(GLubyte) * widthsquared, states, gl::DYNAMIC_DRAW);

        refresh = false;
        UpdateText();
    }

    program->Use();
    gl::DrawArraysInstanced(gl::TRIANGLES, 0, 6, widthsquared);

    font.Render();
}

void MainState::KeyPress(const int key, const bool shift)
{
    const unsigned int modifier = shift ? 10 : 1;

    switch (key)
    {
        case GLFW_KEY_SPACE:
            {
                paused = !paused;
                break;
            }
        case GLFW_KEY_ESCAPE:
            {
                PopState();
                break;
            }
        case GLFW_KEY_BACKSPACE:
            {
                Blank();
                break;
            }
        case GLFW_KEY_ENTER:
        case GLFW_KEY_KP_ENTER:
            {
                Random();
                break;
            }
        case GLFW_KEY_UP:
            {
                frequency += 5 * modifier;
                interval = 1.0 / static_cast<double>(frequency);
                UpdateText();
                break;
            }
        case GLFW_KEY_DOWN:
            {
                if (frequency > 5 * modifier)
                {
                    frequency -= 5 * modifier;
                } else
                {
                    frequency = 1;
                }
                interval = 1.0 / static_cast<double>(frequency);
                UpdateText();
                break;
            }
        case GLFW_KEY_LEFT:
            {
                if (frequency > modifier)
                {
                    frequency -= modifier;
                } else
                {
                    frequency = 1;
                }
                interval = 1.0 / static_cast<double>(frequency);
                UpdateText();
                break;
            }
        case GLFW_KEY_RIGHT:
            {
                frequency += modifier;
                interval = 1.0 / static_cast<double>(frequency);
                UpdateText();
                break;
            }
        default:
            {
                break;
            }
    }
}

void MainState::MousePress(const double x, const double y)
{
    unsigned int xcell = static_cast<unsigned int>(width * x);
    if (xcell >= width)
    {
        xcell = width;
    }
    unsigned int ycell = static_cast<unsigned int>(width * y);
    if (ycell >= width)
    {
        ycell = width;
    }
    const unsigned int cell = ycell * width + xcell + 1;
    lua_rawgeti(lua, 2, cell);
    lua_getfield(lua, 3, "state");
    const GLubyte state = lua_tointeger(lua, 4);
    lua_pop(lua, 1);
    lua_pushinteger(lua, (state + 1) % states.size());
    lua_setfield(lua, 3, "state");
    lua_pop(lua, 1);

    refresh = true;
}

void MainState::WindowSize(const int width, const int height)
{
    font.WindowSize(width, height);
}

void MainState::Random()
{
    std::default_random_engine generator(++iteration);
    std::uniform_int_distribution<GLubyte> distribution(0, states.size() - 1);

    for (unsigned int cell = 1; cell <= widthsquared; ++cell)
    {
        GLubyte state = distribution(generator);

        lua_rawgeti(lua, 2, cell);
        lua_pushinteger(lua, state);
        lua_setfield(lua, 3, "state");
        lua_pushinteger(lua, state);
        lua_setfield(lua, 3, "nextState");
        lua_pop(lua, 1);
    }
    iteration = 0;
    refresh = true;
}

void MainState::Blank()
{
    for (unsigned int cell = 1; cell <= widthsquared; ++cell)
    {
        lua_rawgeti(lua, 2, cell);
        lua_pushinteger(lua, 0);
        lua_setfield(lua, 3, "state");
        lua_pushinteger(lua, 0);
        lua_setfield(lua, 3, "nextState");
        lua_pop(lua, 1);
    }
    iteration = 0;
    refresh = true;
}

void MainState::UpdateText()
{
    std::ostringstream ss;
    ss.imbue(locale);
    ss << "Iteration: " << iteration << "\nFrequency: " << frequency;
    font.SetBuffer(16, 40, 24, ss.str());
}
