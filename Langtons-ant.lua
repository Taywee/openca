--[[
    This file is part of openca.
  
    Copyright (c) 2015, Taylor C. Richberger <taywee@gmx.com>

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted, provided that the above
    copyright notice and this permission notice appear in all copies.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
    SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
    OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
    CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 --]]

local default = 0

states = {{r = 1.0, g = 1.0, b = 1.0}, {r = 0.0, b = 0.0, g = 0.0}, {r = 1.0, b = 0.0, g = 0.0}}

local direction = 5
local antState = 0
local antCell = nil

function RunCells(cells)
    if antCell then
        local nextCell = antCell[direction]
        antCell.state = antState
        if nextCell.state == 0 then
            antState = 1
            direction = direction - 2
            if direction <= 0 then
                direction = direction + 8
            end
        else
            antState = 0
            direction = direction + 2
            if direction >= 9 then
                direction = direction - 8
            end
        end
        nextCell.state = 2
        antCell = nextCell
    else
        for i, cell in ipairs(cells) do
            if cell.state == 2 then
                antCell = cell
            end
        end
    end
end
