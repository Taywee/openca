/*
 * This file is part of openca.
 *
 * Copyright (c) 2015, Taylor C. Richberger <taywee@gmx.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "texturefont.hxx"

#include <fstream>
#include <sstream>
#include <stdexcept>
#include <vector>
#include <string>

#include "shader.hxx"

TextureFont::TextureFont(TextureFontBuffer &texturefontbuffer) : texturefontbuffer(texturefontbuffer), bufLen(0)
{
    std::string vertsrc;
    std::string fragsrc;
    {
        std::ifstream shaderFile("texturefont.vs");
        std::ostringstream buffer;
        buffer << shaderFile.rdbuf();
        vertsrc.assign(buffer.str());
    }
    {
        std::ifstream shaderFile("texturefont.fs");
        std::ostringstream buffer;
        buffer << shaderFile.rdbuf();
        fragsrc.assign(buffer.str());
    }

    Shader vert(gl::VERTEX_SHADER, vertsrc);
    Shader frag(gl::FRAGMENT_SHADER, fragsrc);

    program = std::unique_ptr<Program>(new Program(vert, frag));
    program->Use();

    GLfloat vertices[12] = {
        1.0, 1.0,
        -1.0, 1.0,
        -1.0, -1.0,
        1.0, 1.0,
        -1.0, -1.0,
        1.0, -1.0
    };

    gl::GenVertexArrays(1, &squareVAO);
    gl::BindVertexArray(squareVAO);

    gl::GenBuffers(1, &squareVBO);

    gl::BindBuffer(gl::ARRAY_BUFFER, squareVBO);
    gl::BufferData(gl::ARRAY_BUFFER, sizeof(vertices), vertices, gl::STATIC_DRAW);

    GLint vertAttrib = gl::GetAttribLocation(program->Get(), "vert");
    if (vertAttrib != -1)
    {
        gl::VertexAttribPointer(vertAttrib, 2, gl::FLOAT, gl::FALSE_, 0, 0);
        gl::EnableVertexAttribArray(vertAttrib);
    }

    gl::GenBuffers(1, &charVBO);

    constexpr unsigned int glyphSize = sizeof(int) + (6 * sizeof(float));

    gl::BindBuffer(gl::ARRAY_BUFFER, charVBO);

    vertAttrib = gl::GetAttribLocation(program->Get(), "charIndex");
    if (vertAttrib != -1)
    {
        gl::VertexAttribIPointer(vertAttrib, 1, gl::INT, glyphSize, 0);
        gl::VertexAttribDivisor(vertAttrib, 1);
        gl::EnableVertexAttribArray(vertAttrib);
    }

    if (vertAttrib != -1)
    {
        vertAttrib = gl::GetAttribLocation(program->Get(), "dimensions");
        gl::VertexAttribPointer(vertAttrib, 2, gl::FLOAT, gl::FALSE_, glyphSize, reinterpret_cast<void *>(sizeof(GLint)));
        gl::VertexAttribDivisor(vertAttrib, 1);
        gl::EnableVertexAttribArray(vertAttrib);
    }

    vertAttrib = gl::GetAttribLocation(program->Get(), "bearing");
    if (vertAttrib != -1)
    {
        gl::VertexAttribPointer(vertAttrib, 2, gl::FLOAT, gl::FALSE_, glyphSize, reinterpret_cast<void *>(sizeof(GLint) + (2 * sizeof(GLfloat))));
        gl::VertexAttribDivisor(vertAttrib, 1);
        gl::EnableVertexAttribArray(vertAttrib);
    }

    vertAttrib = gl::GetAttribLocation(program->Get(), "position");
    if (vertAttrib != -1)
    {
        gl::VertexAttribPointer(vertAttrib, 2, gl::FLOAT, gl::FALSE_, glyphSize, reinterpret_cast<void *>(sizeof(GLint) + (4 * sizeof(GLfloat))));
        gl::VertexAttribDivisor(vertAttrib, 1);
        gl::EnableVertexAttribArray(vertAttrib);
    }

    GLint uniform = gl::GetUniformLocation(program->Get(), "tex");
    if (uniform != -1)
    {
        gl::Uniform1i(uniform, 0);
    }

    gl::BindBuffer(gl::ARRAY_BUFFER, 0);
    gl::BindVertexArray(0);
}

TextureFont::~TextureFont()
{
    gl::DeleteBuffers(1, &squareVBO);
    gl::DeleteBuffers(1, &charVBO);
    gl::DeleteVertexArrays(1, &squareVAO);
}

void TextureFont::SetBuffer(const float x, const float y, const float height, const std::string &string)
{
    constexpr unsigned int glyphSize = sizeof(int) + (6 * sizeof(float));
    std::vector<char> buf;
    buf.reserve(string.size() * glyphSize);
    unsigned int horizPos = x;
    unsigned int vertPos = y;
    for (const char c: string)
    {
        if (c == '\n')
        {
            vertPos -= height;
            horizPos = x;
        } else
        {
            const Glyph &glyph = texturefontbuffer.GetGlyph(c);
            if (glyph.index >= 0)
            {
                size_t prevEnd = buf.size();
                buf.insert(buf.end(), glyphSize, 0);
                char * const bufIter = buf.data() + prevEnd;

                *reinterpret_cast<GLint *>(bufIter) = glyph.index;
                *reinterpret_cast<GLfloat *>(bufIter + sizeof(GLint)) = glyph.width;
                *reinterpret_cast<GLfloat *>(bufIter + sizeof(GLint) + sizeof(GLfloat)) = glyph.height;
                *reinterpret_cast<GLfloat *>(bufIter + sizeof(GLint) + (2 * sizeof(GLfloat))) = glyph.left;
                *reinterpret_cast<GLfloat *>(bufIter + sizeof(GLint) + (3 * sizeof(GLfloat))) = glyph.top;
                *reinterpret_cast<GLfloat *>(bufIter + sizeof(GLint) + (4 * sizeof(GLfloat))) = horizPos;
                *reinterpret_cast<GLfloat *>(bufIter + sizeof(GLint) + (5 * sizeof(GLfloat))) = vertPos;
                horizPos += glyph.advance * height;
            } else
            {
                // Still advance even if glyph is invisible
                horizPos += glyph.advance * height;
            }
        }
    }
    gl::BindBuffer(gl::ARRAY_BUFFER, charVBO);
    gl::BufferData(gl::ARRAY_BUFFER, buf.size(), buf.data(), gl::DYNAMIC_DRAW);

    bufLen = buf.size() / glyphSize;

    program->Use();

    GLint uniform = gl::GetUniformLocation(program->Get(), "fontSize");
    gl::Uniform1f(uniform, height);
}

void TextureFont::WindowSize(const unsigned int width, const unsigned int height)
{
    program->Use();

    GLint uniform = gl::GetUniformLocation(program->Get(), "screenSize");
    gl::Uniform2f(uniform, width, height);
}

void TextureFont::SetColor(const float r, const float g, const float b)
{
    program->Use();

    GLint uniform = gl::GetUniformLocation(program->Get(), "color");
    gl::Uniform3f(uniform, r, g, b);
}

void TextureFont::Render()
{
    gl::Disable(gl::DEPTH_TEST);
    gl::BindVertexArray(squareVAO);
    texturefontbuffer.Bind(0);

    program->Use();
    gl::DrawArraysInstanced(gl::TRIANGLES, 0, 6, bufLen);
}
