#include "luastate.hxx"

#include <string>
#include <stdexcept>
#include <iostream>

LuaState::LuaState() : lua(nullptr)
{
    lua = luaL_newstate();
}

/// The state can be moved in a trivial way.
LuaState::LuaState(LuaState &&that)
{
    this->lua = that.lua;
    that.lua = nullptr;
}

/// Close the Lua state if it exists
LuaState::~LuaState()
{
    if (lua)
    {
        lua_close(lua);
    }
}

/// Cast easily to the Lua state for Lua calls
LuaState::operator lua_State *()
{
    return lua;
}

int LuaState::Error(lua_State *l)
{
    return 1;
}
