# This file is part of openca.
#
# Copyright (c) 2015, Taylor C. Richberger <taywee@gmx.com>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
OS = $(shell uname -s)

CC=cc
CXX=c++
DESTDIR=/usr
DEFINES=
FLAGS=-std=c++11
LIBS=glfw3 luajit freetype2
CFLAGS=$(FLAGS) -c -O3 -MMD `pkg-config --cflags $(LIBS)`
LDFLAGS=$(FLAGS) `pkg-config --libs $(LIBS)`
ifeq ($(OS), Darwin)
LDFLAGS+=-pagezero_size 10000 -image_base 100000000
else
LDFLAGS+=-s -lGL
endif
SOURCES=main.cxx gl_core_3_3.cxx gamecore.cxx state.cxx mainstate.cxx shader.cxx program.cxx luastate.cxx texturefont.cxx texturefontbuffer.cxx menustate.cxx
OBJECTS=$(SOURCES:.cxx=.o)
DEPENDENCIES=$(SOURCES:.cxx=.d)
EXECUTABLE=openca

.PHONY: all clean

all: $(EXECUTABLE)

-include $(DEPENDENCIES)

$(EXECUTABLE): $(OBJECTS)
	$(CXX) -o $@ $(OBJECTS) $(LDFLAGS)

clean :
	rm $(EXECUTABLE) $(OBJECTS) $(DEPENDENCIES)

%.o: %.cxx
	$(CXX) $< -o $@ $(CFLAGS) $(DEFINES)
