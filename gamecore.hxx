/*
 * This file is part of openca.
 *
 * Copyright (c) 2015, Taylor C. Richberger <taywee@gmx.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#pragma once

#include <vector>
#include <memory>
#include <tuple>
#include <utility>

#include "gl_core_3_3.hxx"
#include <GLFW/glfw3.h>

#include "texturefontbuffer.hxx"

class State;

class GameCore
{
    private:
        GLFWwindow *window;
        double frameTime;
        double runTime;

        std::tuple<int, int> windowSize;

        std::unique_ptr<TextureFontBuffer> texturefontbuffer;

        std::vector<std::unique_ptr<State>> states;

        // A temporary structure for popped states to be killed later
        std::vector<std::unique_ptr<State>> deadStates;

    public:
        GameCore();
        ~GameCore();
        void Run();
        void Process();
        void Render();

        void GLErrors();

        void PushState(State *state);
        void PopState();

        const std::tuple<int, int> &GetWindowSize();

        template <typename ...Args>
        struct Callback
        {
            template <void(GameCore::* Fun)(Args...)>
            static void Function(GLFWwindow *window, Args ...args)
            {
                GameCore *game = reinterpret_cast<GameCore *>(glfwGetWindowUserPointer(window));
                (game->*Fun)(std::forward<Args>(args)...);
            }
        };

        // Stays static because the program has no window at this point to pull user data from
        static void ErrorCallback(int error, const char * description);

        //void CharCallback(unsigned int codepoint);
        //void CharModsCallback(unsigned int codepoint, unsigned int mods);
        //void CursorEnterCallback(int entered);
        //void CursorPosCallback(double xpos, double ypos);
        //void DropCallback(int count, const char ** poths);
        //void FramebufferSizeCallback(int width, int height);
        void KeyCallback(int key, int scancode, int action, int mods);
        //void MonitorCallback(GLFWmonitor *monitor, int event);
        void MouseButtonCallback(int button, int action, int mods);
        //void ScrollCallback(int xoffset, int yoffset);
        //void WindowCloseCallback();
        //void WindowFocusCallback(int focused);
        //void WindowIconifyCallback(int iconified);
        //void WindowPosCallback(int xpos, int ypos);
        //void WindowRefreshCallback();
        void WindowSizeCallback(int width, int height);
};
