/*
 * This file is part of openca.
 *
 * Copyright (c) 2015, Taylor C. Richberger <taywee@gmx.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "texturefontbuffer.hxx"

#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>

#include <ft2build.h>
#include FT_FREETYPE_H

static std::string GetFTErrorMessage(const FT_Error error)
{
    #undef __FTERRORS_H__
    #define FT_ERRORDEF( e, v, s )  case e: return std::string(s);
    #define FT_ERROR_START_LIST     switch (error) {
    #define FT_ERROR_END_LIST       }
    #include FT_ERRORS_H
    return std::string("(Unknown error)");
}

TextureFontBuffer::TextureFontBuffer(const std::string &fontPath, const unsigned int size, const bool extendedUnicode) 
{
    FT_Library library;
    FT_Face face;
    FT_Error error = FT_Init_FreeType(&library);
    if (error)
    {
        throw std::runtime_error("Could not run FT_Init_FreeType: " + GetFTErrorMessage(error));
    }
    error = FT_New_Face(library, fontPath.c_str(), 0, &face);
    if (error)
    {
        throw std::runtime_error("Could not run FT_New_Face: " + GetFTErrorMessage(error));
    }
    error = FT_Select_Charmap(face, FT_ENCODING_UNICODE);
    if (error)
    {
        throw std::runtime_error("Could not run FT_Select_Charmap: " + GetFTErrorMessage(error));
    }
    std::vector<unsigned char> texVec;

    const unsigned int sizeSquared = size * size;

    error = FT_Set_Pixel_Sizes(face, size, size);
    if (error)
    {
        throw std::runtime_error("Could not run FT_Set_Pixel_Sizes: " + GetFTErrorMessage(error));
    }


    // Load up texVec with all glyphs, and load up glyphmap with metrics and indices
    // Leave top-to-bottom; the texture coordinates can simply be flipped, not worth reordering the entire bitmap.
    FT_UInt glyph_index;
    for (FT_ULong charcode = FT_Get_First_Char(face, &glyph_index); glyph_index != 0; charcode = FT_Get_Next_Char(face, charcode, &glyph_index))
    {
        if (extendedUnicode || charcode < 256)
        {

            error = FT_Load_Glyph(face, glyph_index, FT_LOAD_RENDER);
            if (error)
            {
                throw std::runtime_error("Could not run FT_Load_Glyph: " + GetFTErrorMessage(error));
            }

            Glyph &glyph = glyphmap[charcode];
            const FT_Glyph_Metrics &metrics = face->glyph->metrics;

            const unsigned int rows = face->glyph->bitmap.rows;
            const unsigned int width = face->glyph->bitmap.width;
            const unsigned int pitch = face->glyph->bitmap.pitch;

            // All glyph metrics are scaled into unit form, so any size can be used
            glyph.advance = static_cast<float>(face->glyph->advance.x >> 6) / static_cast<float>(size);
            glyph.left = static_cast<float>(face->glyph->bitmap_left) / static_cast<float>(size);
            glyph.top = static_cast<float>(face->glyph->bitmap_top) / static_cast<float>(size);
            glyph.width = static_cast<float>(width) / static_cast<float>(size);
            glyph.height = static_cast<float>(rows) / static_cast<float>(size);

            if (width > 0 && rows > 0)
            {
                unsigned long base = texVec.size();
                glyph.index = base / sizeSquared;
                texVec.resize(texVec.size() + sizeSquared);

                for (unsigned int y = 0; y < size; ++y)
                {
                    for (unsigned int x = 0; x < size; ++x)
                    {
                        if (y < rows && x < width)
                        {
                            texVec.at(base++) = face->glyph->bitmap.buffer[x + (y * pitch)];
                        } else
                        {
                            ++base;
                        }
                    }
                }
            } else
            {
                glyph.index = -1;
            }
        }
    }

    // Done with freetype, since we've rendered all the glyphs
    error = FT_Done_Face(face);
    if (error)
    {
        throw std::runtime_error("Could not run FT_Done_Face: " + GetFTErrorMessage(error));
    }
    error = FT_Done_FreeType(library);
    if (error)
    {
        throw std::runtime_error("Could not run FT_Done_FreeType: " + GetFTErrorMessage(error));
    }

    gl::GenTextures(1, &texture);
    gl::BindTexture(gl::TEXTURE_2D_ARRAY, texture);
    gl::TexParameteri(gl::TEXTURE_2D_ARRAY, gl::TEXTURE_MIN_FILTER, gl::LINEAR_MIPMAP_LINEAR);
    gl::TexParameteri(gl::TEXTURE_2D_ARRAY, gl::TEXTURE_MAG_FILTER, gl::LINEAR);
    gl::TexParameteri(gl::TEXTURE_2D_ARRAY, gl::TEXTURE_WRAP_S, gl::CLAMP_TO_BORDER);
    gl::TexParameteri(gl::TEXTURE_2D_ARRAY, gl::TEXTURE_WRAP_T, gl::CLAMP_TO_BORDER);
    float clear[] = { 0.0f, 0.0f, 0.0f, 0.0f };
    gl::TexParameterfv(gl::TEXTURE_2D_ARRAY, gl::TEXTURE_BORDER_COLOR, clear);

    gl::TexImage3D(gl::TEXTURE_2D_ARRAY, 0, gl::R8, size, size, texVec.size() / sizeSquared, 0, gl::RED, gl::UNSIGNED_BYTE, texVec.data());
    gl::GenerateMipmap(gl::TEXTURE_2D_ARRAY);
}

TextureFontBuffer::~TextureFontBuffer()
{
    gl::DeleteTextures(1, &texture);
}

const GLuint TextureFontBuffer::Get()
{
    return texture;
}

void TextureFontBuffer::Bind(const GLuint tex)
{
    gl::ActiveTexture(gl::TEXTURE0 + tex);
    gl::BindTexture(gl::TEXTURE_2D_ARRAY, texture);
}

const Glyph &TextureFontBuffer::GetGlyph(unsigned long codepoint)
{
    return(glyphmap.at(codepoint));
}
