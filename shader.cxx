/*
 * This file is part of openca.
 *
 * Copyright (c) 2015, Taylor C. Richberger <taywee@gmx.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "shader.hxx"

#include <vector>
#include <string>
#include <stdexcept>
#include <iostream>

Shader::Shader(const GLenum shaderType, const std::string &source)
{
    shader = gl::CreateShader(shaderType);

    // This is the easiest way to do it without risking memory leak or making unnecessary copies
    const GLchar * sourcePointer = reinterpret_cast<const GLchar *>(source.data());
    const GLint sourceLength = source.length();
    gl::ShaderSource(shader, 1, &sourcePointer, &sourceLength);
    gl::CompileShader(shader);

    GLint compiled = 0;
    gl::GetShaderiv(shader, gl::COMPILE_STATUS, &compiled);
    if (!compiled)
    {
        GLint logLength = 0;
        gl::GetShaderiv(shader, gl::INFO_LOG_LENGTH, &logLength);

        std::vector<GLchar> errorLog(logLength, '\0');
        gl::GetShaderInfoLog(shader, logLength, nullptr, errorLog.data());
        while (errorLog.back() == '\0')
        {
            errorLog.pop_back();
        }

        std::string error("Could not compile shader:\n");
        error.append(errorLog.begin(), errorLog.end());

        gl::DeleteShader(shader);

        throw std::runtime_error(error);
    }
}

Shader::~Shader()
{
    gl::DeleteShader(shader);
}

