/*
 * This file is part of openca.
 *
 * Copyright (c) 2015, Taylor C. Richberger <taywee@gmx.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#pragma once

#include <string>
#include <memory>
#include <unordered_map>

#include "gl_core_3_3.hxx"
#include <GLFW/glfw3.h>

#include "program.hxx"
#include "texturefontbuffer.hxx"

class TextureFont
{
    private:
        std::unique_ptr<Program> program;
        GLuint squareVAO;
        GLuint squareVBO;
        GLuint charVBO;

        TextureFontBuffer &texturefontbuffer;

        unsigned int bufLen;

    public:
        TextureFont(TextureFontBuffer &texturefontbuffer);

        ~TextureFont();

        void SetBuffer(const float x, const float y, const float height, const std::string &string);
        void WindowSize(const unsigned int width, const unsigned int height);
        void SetColor(const float r, const float g, const float b);
        void Render();
};
