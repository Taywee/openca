/*
 * This file is part of openca.
 *
 * Copyright (c) 2015, Taylor C. Richberger <taywee@gmx.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#pragma once

#include <string>
#include <unordered_map>

#include "gl_core_3_3.hxx"
#include <GLFW/glfw3.h>


struct Glyph
{
    // Index of glyph in texture array, <0 is blank
    long index;
    float width;
    float height;
    float left;
    float top;
    float advance;
};

class TextureFontBuffer
{
    private:
        GLuint texture;

        // Map from unicode codepoint -> glyph structure
        std::unordered_map<unsigned long, Glyph> glyphmap;

    public:
        TextureFontBuffer(const std::string &fontPath, const unsigned int size = 128, const bool extendedUnicode = false);

        ~TextureFontBuffer();

        const GLuint Get();
        void Bind(const GLuint tex);

        const Glyph &GetGlyph(unsigned long codepoint);
};
