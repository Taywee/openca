/*
 * This file is part of openca.
 *
 * Copyright (c) 2015, Taylor C. Richberger <taywee@gmx.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#pragma once

#include <memory>
#include <vector>
#include <tuple>
#include <locale>

#include "state.hxx"
#include "program.hxx"
#include "luastate.hxx"
#include "texturefont.hxx"
#include "texturefontbuffer.hxx"

class MainState : public State
{
    private:
        const std::locale locale;

        TextureFont font;
        std::unique_ptr<Program> program;
        GLuint squareVAO;
        GLuint squareVBO;
        GLuint stateVBO;

        LuaState lua;
        unsigned long long iteration;

        const unsigned int width;
        const unsigned int widthsquared;

        unsigned int frequency;
        double interval;
        double passedTime;

        bool paused;
        bool refresh;

        std::vector<std::tuple<float, float, float>> states;

    public:
        MainState(GameCore *game, TextureFontBuffer &texturefontbuffer, const std::string &automaton, const unsigned int width, const bool wrap);
        virtual ~MainState();

        virtual void Pause() override;
        virtual void Resume() override;

        virtual void Process(double frameTime) override;
        virtual void Render() override;

        virtual void KeyPress(const int key, const bool shift) override;
        virtual void MousePress(const double x, const double y) override;

        virtual void WindowSize(int width, int height) override;

        // Randomize entire arena
        void Random();

        // Blank entire arena
        void Blank();

        void UpdateText();
};
