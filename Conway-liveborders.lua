--[[
    This file is part of openca.
  
    Copyright (c) 2015, Taylor C. Richberger <taywee@gmx.com>

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted, provided that the above
    copyright notice and this permission notice appear in all copies.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
    SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
    OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
    CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 --]]

local default = 1

states = {{r = 1.0, g = 1.0, b = 1.0}, {r = 0.0, b = 0.0, g = 0.0}}

local CountLiveNeighbors = function(cell)
    local count = 0
    for i, neighbor in ipairs(cell) do
        if neighbor then
            count = count + neighbor.state
        else
            count = count + default
        end
    end
    return count
end

local function RunCell(cell)
    local environment = CountLiveNeighbors(cell)
    local state = cell.state

    if state == 0 then
        if environment == 3 then
            cell.nextState = 1
        end
    else
        if environment < 2 then
            cell.nextState = 0
        elseif environment > 3 then
            cell.nextState = 0
        else
            cell.nextState = 1
        end
    end
end

function RunCells(cells)
    for i, cell in ipairs(cells) do
        RunCell(cell)
    end
    for i, cell in ipairs(cells) do
        cell.state = cell.nextState
    end
end
