/*
 * This file is part of openca.
 *
 * Copyright (c) 2015, Taylor C. Richberger <taywee@gmx.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "menustate.hxx"

#include "mainstate.hxx"

#include <sstream>
#include <tuple>
#include <stdexcept>
#include <iostream>

#include <sys/types.h>
#include <dirent.h>

MenuState::MenuState(GameCore *game, TextureFontBuffer &texturefontbuffer) : State(game), font(texturefontbuffer), locale(""), startFont(texturefontbuffer), automatonFont(texturefontbuffer), wrapFont(texturefontbuffer), exitFont(texturefontbuffer), instructionsFont(texturefontbuffer), currentItem(0), wrap(true), width(50)
{
    const std::tuple<int, int> &windowSize = GetWindowSize();
    startFont.SetColor(0.5, 0.5, 0.5);
    startFont.WindowSize(std::get<0>(windowSize), std::get<1>(windowSize));
    automatonFont.SetColor(0.5, 0.5, 0.5);
    automatonFont.WindowSize(std::get<0>(windowSize), std::get<1>(windowSize));
    wrapFont.SetColor(1.0, 1.0, 1.0);
    wrapFont.WindowSize(std::get<0>(windowSize), std::get<1>(windowSize));
    exitFont.SetColor(0.5, 0.5, 0.5);
    exitFont.WindowSize(std::get<0>(windowSize), std::get<1>(windowSize));
    instructionsFont.SetColor(1.0, 1.0, 1.0);
    instructionsFont.WindowSize(std::get<0>(windowSize), std::get<1>(windowSize));

    exitFont.SetBuffer(16, 390, 24, "Exit");
    instructionsFont.SetBuffer(16, 318, 24, 
            "Left Click = Change cell state\n"
            "Enter = Randomize, Backspace = Clear\n"
            "Space = Play/Pause\n"
            "Left/Right = De/Increase frequency 1\n"
            "Down/Up = De/Increase frequency 5\n"
            "Shift+Left/Right = De/Increase frequency 10\n"
            "Shift+Down/Up = De/Increase frequency 50\n"
            "Escape = exit");

    // Set up automaton structure
    
    DIR *dir = opendir(".");
    if (dir == nullptr)
    {
        throw std::runtime_error("Could not open directory");
    }
    struct dirent *entry;
    while ((entry = readdir(dir)))
    {
        std::string file(entry->d_name);
        if (file.rfind(".lua") == (file.size() - 4))
        {
            automatons.emplace_back(std::move(file));
        }
    }
    if (automatons.empty())
    {
        throw std::runtime_error("Could not find any automatons");
    }

    selectedAutomaton = automatons.begin();
}

MenuState::~MenuState()
{
}

void MenuState::Pause()
{
}

void MenuState::Resume()
{
    const std::tuple<int, int> &windowSize = GetWindowSize();
    startFont.WindowSize(std::get<0>(windowSize), std::get<1>(windowSize));
    automatonFont.WindowSize(std::get<0>(windowSize), std::get<1>(windowSize));
    wrapFont.WindowSize(std::get<0>(windowSize), std::get<1>(windowSize));
    exitFont.WindowSize(std::get<0>(windowSize), std::get<1>(windowSize));
    instructionsFont.WindowSize(std::get<0>(windowSize), std::get<1>(windowSize));
}

void MenuState::Process(double frameTime)
{
    startFont.SetColor(0.5, 0.5, 0.5);
    automatonFont.SetColor(0.5, 0.5, 0.5);
    wrapFont.SetColor(0.5, 0.5, 0.5);
    exitFont.SetColor(0.5, 0.5, 0.5);
    switch (currentItem)
    {
        case 0:
            startFont.SetColor(1.0, 1.0, 1.0);
            break;
        case 1:
            automatonFont.SetColor(1.0, 1.0, 1.0);
            break;
        case 2:
            wrapFont.SetColor(1.0, 1.0, 1.0);
            break;
        case 3:
            exitFont.SetColor(1.0, 1.0, 1.0);
            break;
    }
    std::ostringstream startString;
    startString.imbue(locale);
    startString << "Start Game With Width: " << width;
    startFont.SetBuffer(16, 462, 24, startString.str());

    std::ostringstream automatonString;
    automatonString.imbue(locale);
    automatonString << "Use Automaton: " << *selectedAutomaton;
    automatonFont.SetBuffer(16, 438, 24, automatonString.str());

    std::ostringstream wrapString;
    wrapString.imbue(locale);
    wrapString << "Wrap: " << (wrap ? "ON" : "OFF");
    wrapFont.SetBuffer(16, 414, 24, wrapString.str());

}

void MenuState::Render()
{
    startFont.Render();
    automatonFont.Render();
    wrapFont.Render();
    exitFont.Render();
    instructionsFont.Render();
}

void MenuState::KeyPress(const int key, const bool shift)
{
    const unsigned int modifier = shift ? 10 : 1;
    signed char itemchange = 0;
    switch (key)
    {
        case GLFW_KEY_ESCAPE:
            {
                PopState();
                break;
            }
        case GLFW_KEY_ENTER:
        case GLFW_KEY_KP_ENTER:
            {
                switch (currentItem)
                {
                    case 0:
                        PushState(new MainState(game, font, *selectedAutomaton, width, wrap));
                        break;
                    case 1:
                        ++selectedAutomaton;
                        if (selectedAutomaton == automatons.end())
                        {
                            selectedAutomaton = automatons.begin();
                        }
                        break;
                    case 2:
                        wrap = !wrap;
                        break;
                    case 3:
                        PopState();
                        break;
                }
                break;
            }
        case GLFW_KEY_UP:
            {
                itemchange = -1;
                break;
            }
        case GLFW_KEY_DOWN:
            {
                itemchange = 1;
                break;
            }
        case GLFW_KEY_LEFT:
            {
                switch (currentItem)
                {
                    case 0:
                        if (width > modifier)
                        {
                            width -= modifier;
                        } else
                        {
                            width = 1;
                        }
                        break;

                    case 1:
                        if (selectedAutomaton == automatons.begin())
                        {
                            selectedAutomaton = automatons.end();
                        }
                        --selectedAutomaton;
                        break;

                    case 2:
                        wrap = !wrap;
                        break;
                }
                break;
            }
        case GLFW_KEY_RIGHT:
            {
                switch (currentItem)
                {
                    case 0:
                        width += modifier;
                        break;

                    case 1:
                        ++selectedAutomaton;
                        if (selectedAutomaton == automatons.end())
                        {
                            selectedAutomaton = automatons.begin();
                        }
                        break;

                    case 2:
                        wrap = !wrap;
                        break;
                }
                break;
            }
        default:
            {
                break;
            }
    }
    currentItem = (currentItem + menuitems + itemchange) % menuitems;
}

void MenuState::WindowSize(const int width, const int height)
{
    startFont.WindowSize(width, height);
    automatonFont.WindowSize(width, height);
    wrapFont.WindowSize(width, height);
    exitFont.WindowSize(width, height);
    instructionsFont.WindowSize(width, height);
}
